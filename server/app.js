const express = require('express')
const app = express()
const port = 8085

app.set('port', process.env.PORT || port);

app.use(express.static('public'))

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.get('/', function (req, res) {
  res.send('Hello,World!')
})

let fortunes = ["win the lotto", "fall in love", "fall out of love", "fall on the ground", "donate generously to the Northwest Alumni Association"]

app.get('/fortune', function(req,res){
  let choice = Math.floor(Math.random()*fortunes.length)
  res.send("You will " + fortunes[choice])
})


app.listen(app.get('port'), function () {
  console.log('app listening on localhost:'+port)
})